# GITLAB Pre-defined variables: https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
# --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"

variables:
  SECURE_LOG_LEVEL: debug
  CI_DEBUG_TRACE: "true"
  DOCKER_IMAGE_NAME: docker.io/umeshkumar01/my-static-site1
  DOCKER_TAG: ${CI_COMMIT_SHORT_SHA}
  DOCKER_REGISTRY_URL_AUTH: https://index.docker.io/v1/
  

  
stages:
  - build
  - test
  - deploy

build-with-dind:
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  stage: build
  script:
    - docker build -t ${DOCKER_IMAGE_NAME}:${DOCKER_TAG} .
    - docker login -u ${Docker_Hub_Username} -p ${Docker_Hub_Token}
    - docker push ${DOCKER_IMAGE_NAME}:${DOCKER_TAG}

build with kaniko:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}"
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"

test a:
    stage: test
    script:
      - echo "This job tests something. It will only run when all jobs in the"
      - echo "build stage are complete."
deploy_a:
  stage: deploy
  script:
    - echo "this job 2 for you. It will only run when all jobs in the"
    - echo "test stage complete."
  environment: production
