FROM python
WORKDIR /usr/src/myapp
COPY . /usr/src/myapp
EXPOSE 80
CMD ["python3","main.py"]
